<?php

Route::get('/', function(){return redirect('/home');});
Route::get('/home', 'UserController@home')->name('home');
Route::get('/blog', 'UserController@blog')->name('blog');
Route::get('/blog/{slug}', 'UserController@show_article')->name('blog.show');
Route::get('/destination', 'UserController@destination')->name('destination');
Route::get('/destination/{slug}', 'UserController@show_destination')->name('destination.show');
Route::get('/produk/{slug}', 'UserController@show_produk')->name('user.show_produk');
Route::get('/artikel/{slug}', 'UserController@show_artikel')->name('user.show_artikel');
Route::get('/contact', 'UserController@contact')->name('contact');
Route::get('/slide', 'UserController@slide')->name('slide');

Route::prefix('admin')->group(function(){

  Route::get('/', function(){
    return view('auth/login');
  });

  // handle route register
  Route::match(["GET", "POST"], "/register", function(){
    return redirect("/login");
  })->name("register");

  Auth::routes();

  // Route Dashboard
  Route::get('/dashboard', 'DashboardController@index')->middleware('auth');

  // route categories
  Route::get('/categories/{category}/restore', 'CategoryController@restore')->name('categories.restore');
  Route::delete('/categories/{category}/delete-permanent', 'CategoryController@deletePermanent')->name('categories.delete-permanent');
  Route::get('/ajax/categories/search', 'CategoryController@ajaxSearch');
  Route::resource('categories', 'CategoryController')->middleware('auth');

  // route article
  Route::post('/articles/upload', 'ArticleController@upload')->name('articles.upload')->middleware('auth');
  Route::resource('/articles', 'ArticleController')->middleware('auth');

  // route destination
  Route::resource('/destinations', 'DestinationController')->middleware('auth');

  // Route about
  Route::get('/abouts', 'AboutController@index')->name('abouts.index')->middleware('auth');
  Route::get('/abouts/{about}/edit', 'AboutController@edit')->name('abouts.edit')->middleware('auth');
  Route::put('/abouts/{about}', 'AboutController@update')->name('abouts.update')->middleware('auth');
  // Route::resource('abouts', 'AboutController')->middleware('auth');

    // Route slide
    Route::resource('/slide', 'SlideController')->middleware('auth');
    Route::get('/slide', 'SlideController@index')->name('slide.index')->middleware('auth');
    Route::get('/slide/{slide}/edit', 'SlideController@edit')->name('slide.edit')->middleware('auth');
    Route::put('/slide/{slide}', 'SlideController@update')->name('slide.update')->middleware('auth');

    // Route online
    Route::resource('/online', 'OnlineController')->middleware('auth');
    Route::get('/online', 'OnlineController@index')->name('online.index')->middleware('auth');
    Route::get('/online/{online}/edit', 'OnlineController@edit')->name('online.edit')->middleware('auth');
    Route::put('/online/{online}', 'OnlineController@update')->name('online.update')->middleware('auth');

    // Route sosmed
    Route::resource('/sosmed', 'SosmedController')->middleware('auth');
    Route::get('/sosmed', 'SosmedController@index')->name('sosmed.index')->middleware('auth');
    Route::get('/sosmed/{sosmed}/edit', 'SosmedController@edit')->name('sosmed.edit')->middleware('auth');
    Route::put('/sosmed/{sosmed}', 'SosmedController@update')->name('sosmed.update')->middleware('auth');

    // Route layout
    Route::resource('/layout', 'LayoutController')->middleware('auth');
    Route::get('/layout', 'LayoutController@index')->name('layout.index')->middleware('auth');
    Route::get('/layout/{layout}/edit', 'LayoutController@edit')->name('layout.edit')->middleware('auth');
    Route::put('/layout/{layout}', 'LayoutController@update')->name('layout.update')->middleware('auth');

    // Route layout
    Route::resource('/produk', 'ProdukController')->middleware('auth');
    Route::get('/produk', 'ProdukController@index')->name('produk.index')->middleware('auth');
    Route::get('/produk/{produk}/edit', 'ProdukController@edit')->name('produk.edit')->middleware('auth');
    Route::put('/produk/{produk}', 'ProdukController@update')->name('produk.update')->middleware('auth');

    // Route artikel
    Route::resource('/artikel', 'ArtikelController')->middleware('auth');
    Route::get('/artikel', 'ArtikelController@index')->name('artikel.index')->middleware('auth');
    Route::get('/artikel/{artikel}/edit', 'ArtikelController@edit')->name('artikel.edit')->middleware('auth');
    Route::put('/artikel/{artikel}', 'ArtikelController@update')->name('artikel.update')->middleware('auth');
});
