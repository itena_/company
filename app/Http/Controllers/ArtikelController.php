<?php

namespace App\Http\Controllers;

use App\Artikel;
use Illuminate\Http\Request;
use File;

class ArtikelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $status = $request->get('status');
        $keyword = $request->get('keyword') ? $request->get('keyword') : '';
        $category = $request->get('c') ? $request->get('c') : '';

        if ($status) {
            $artikel = Artikel::where('status', strtoupper($status))
                ->paginate(10);

        } else {
            $artikel = Artikel::where('title', 'LIKE', "%$keyword%")
                ->paginate(10);
        }

        return view('artikel.index', compact('artikel'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('artikel.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        \Validator::make($request->all(), [
            'title' => 'required|min:2|max:200',
            'image' => 'required',
        ])->validate();

        $new_Artikel = new \App\Artikel();
        $new_Artikel->title = $request->get('title');
        $new_Artikel->kategori = $request->get('kategori');
        $new_Artikel->description = $request->get('description');
        $new_Artikel->weight = $request->get('weight');
        $new_Artikel->price = $request->get('price');

        if ($request->file('image')) {
            $nama_file = time() . "_" . $request->file('image')->getClientOriginalName();
            $image_path = $request->file('image')->move('artikel_image', $nama_file);
            $new_Artikel->image = $nama_file;
        }
        $new_Artikel->save();

        return redirect()->route('artikel.index')->with('success', 'Product successfully created');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
//    public function show($id)
//    {
//
//    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $artikel = Artikel::findOrFail($id);
        return view('artikel.edit', compact('artikel'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $artikel = Artikel::findOrFail($id);

        $artikel->title = $request->get('title');
        $artikel->kategori = $request->get('kategori');
        $artikel->description = $request->get('description');
        $artikel->weight = $request->get('weight');
        $artikel->price = $request->get('price');

        if ($request->file('image')) {
            if ($artikel->image) {
                File::delete('artikel_image/' . $artikel->image);
            }
            $nama_file = time() . "_" . $request->file('artikel')->getClientOriginalName();
            $new_image = $request->file('image')->move('artikel_image', $nama_file);
            $artikel->image = $nama_file;
        }

        $artikel->save();
        return redirect()->route('artikel.index')->with('success', 'Product successfully update.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $artikel = Artikel::findOrFail($id);
        if ($artikel->image) {
            File::delete('artikel_image/' . $artikel->image);
        }
        $artikel->forceDelete();

        return redirect()->route('artikel.index')->with('success', 'Product successfully deleted.');
    }
}

