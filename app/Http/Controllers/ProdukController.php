<?php

namespace App\Http\Controllers;
use App\Produk;
use Illuminate\Http\Request;
use File;

class ProdukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $status     = $request->get('status');
        $keyword    = $request->get('keyword') ? $request->get('keyword') : '';
        $category   = $request->get('c') ? $request->get('c') : '';

        if($status){
            $produk = Produk::where('status', strtoupper($status))
                ->paginate(10);

        }else{
            $produk = Produk::where('title', 'LIKE', "%$keyword%")
                ->paginate(10);
        }

        return view('produk.index', compact('produk'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('produk.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        \Validator::make($request->all(),[
            'title'      => 'required|min:2|max:200',
            'image'      => 'required',
        ])->validate();

        $new_Produk               = new \App\Produk();
        $new_Produk->title        = $request->get('title');
        $new_Produk->kategori        = $request->get('kategori');
        $new_Produk->description        = $request->get('description');
        $new_Produk->weight        = $request->get('weight');
        $new_Produk->price        = $request->get('price');

        if($request->file('image')){
            $nama_file = time()."_".$request->file('image')->getClientOriginalName();
            $image_path = $request->file('image')->move('produk_image', $nama_file);
            $new_Produk->image = $nama_file;
        }
        $new_Produk->save();

        return redirect()->route('produk.index')->with('success', 'Product successfully created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
//    public function show($id)
//    {
//
//    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $produk = Produk::findOrFail($id);
        return view('produk.edit', compact('produk'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $produk = Produk::findOrFail($id);

        $produk->title        = $request->get('title');
        $produk->kategori        = $request->get('kategori');
        $produk->description        = $request->get('description');
        $produk->weight        = $request->get('weight');
        $produk->price        = $request->get('price');

        if($request->file('image')){
            if($produk->image){
                File::delete('produk_image/'.$produk->image);
            }
            $nama_file = time()."_".$request->file('image')->getClientOriginalName();
            $new_image = $request->file('image')->move('produk_image', $nama_file);
            $produk->image = $nama_file;
        }

        $produk->save();
        return redirect()->route('produk.index')->with('success', 'Product successfully update.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $produk = Produk::findOrFail($id);
        if($produk->image){
            File::delete('produk_image/'.$produk->image);
        }
        $produk->forceDelete();

        return redirect()->route('produk.index')->with('success', 'Product successfully deleted.');
    }
}

