<?php

namespace App\Http\Controllers;
use App\Online;
use App\About;
use Illuminate\Http\Request;
use File;

class OnlineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $status     = $request->get('status');
        $keyword    = $request->get('keyword') ? $request->get('keyword') : '';
        $category   = $request->get('c') ? $request->get('c') : '';

        if($status){
            $online = Online::where('status', strtoupper($status))
                ->paginate(10);

        }else{
            $online = Online::where('title', 'LIKE', "%$keyword%")
                ->paginate(10);
        }

        return view('online.index', compact('online'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('online.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        \Validator::make($request->all(),[
            'title'      => 'required|min:2|max:200',
            'image'      => 'required',
        ])->validate();

        $new_Online               = new \App\Online();
        $new_Online->title        = $request->get('title');

        if($request->file('image')){
            $nama_file = time()."_".$request->file('image')->getClientOriginalName();
            $image_path = $request->file('image')->move('online_image', $nama_file);
            $new_Online->image = $nama_file;
        }


        $new_Online->save();

        return redirect()->route('online.index')->with('success', 'Online successfully created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $online = Online::findOrFail($id);
        return view('online.edit', compact('online'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $online = Online::findOrFail($id);

        $online->title        = $request->get('title');

        if($request->file('image')){
            if($online->image){
                File::delete('online_image/'.$online->image);
            }
            $nama_file = time()."_".$request->file('image')->getClientOriginalName();
            $new_image = $request->file('image')->move('online_image', $nama_file);
            $online->image = $nama_file;
        }

        $online->save();
        return redirect()->route('online.index')->with('success', 'Online successfully update.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $online = Online::findOrFail($id);
        if($online->image){
            File::delete('online_image/'.$online->image);
        }
        $online->forceDelete();

        return redirect()->route('online.index')->with('success', 'Online successfully deleted.');
    }
}

