<?php

namespace App\Http\Controllers;
use App\Layout;
use Illuminate\Http\Request;
use File;

class LayoutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $status     = $request->get('status');
        $keyword    = $request->get('keyword') ? $request->get('keyword') : '';

        if($status){
            $layout = Layout::where('status', strtoupper($status))
                ->paginate(10);

        }else{
            $layout = Layout::where('id', 'NOT LIKE', "6")
                ->paginate(10);
        }

        return view('layout.index', compact('layout'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('layout.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $new_Layout               = new \App\Layout();
        $new_Layout->title        = $request->get('title');

        if($request->file('fill')){
            $nama_file = time()."_".$request->file('fill')->getClientOriginalName();
            $image_path = $request->file('fill')->move('layout_image', $nama_file);
            $new_Layout->fill = $nama_file;
        }

        $new_Layout->save();

        return redirect()->route('layout.index')->with('success', 'Layout successfully created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $layout = Layout::findOrFail($id);
        return view('layout.edit', compact('layout'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $layout = Layout::findOrFail($id);

        $layout->title        = $request->get('title');
//        $layout->fill        = $request->get('fill');

        if($request->file('fill')){
            if($layout->fill){
                File::delete('layout_image/'.$layout->fill);
            }
            $nama_file = time()."_".$request->file('fill')->getClientOriginalName();
            $new_image = $request->file('fill')->move('layout_image', $nama_file);
            $layout->fill = $nama_file;
        }

        $layout->save();
        return redirect()->route('layout.index')->with('success', 'Layout successfully update.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $layout = Layout::findOrFail($id);
        $layout->forceDelete();

        return redirect()->route('layout.index')->with('success', 'Layout successfully deleted.');
    }
}

