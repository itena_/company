<?php

namespace App\Http\Controllers;


use App\Produk;
use App\Artikel;
use App\Sosmed;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Category;
use App\About;
use App\Article;
use App\Destination;
use App\Slide;
use App\Online;
use App\Layout;

class UserController extends Controller
{

    public function __construct()
    {
        // $this->middleware('auth');
    }


    public function home()
    {
        $title = Layout::select('fill')->where('title', 'name')->get();
        $logo = Layout::select('fill')->where('title', 'logo')->get();
        $address = Layout::select('fill')->where('title', 'alamat')->get();
        $tlp = Layout::select('fill')->where('title', 'tlp')->get();
        $email = Layout::select('fill')->where('title', 'email')->get();
        $wa = Sosmed::select('title')->where('id', '9')->get();
        $data = [
            'categories' => Category::all(),
            'about' => About::all(),
            'slide' => Slide::all(),
            'online' => Online::all(),
            'sosmed' => Sosmed::all(),
            'produk' => Produk::all(),
            'artikel' => Artikel::all(),
            'title' => $title,
            'address' => $address,
            'tlp' => $tlp,
            'email' => $email,
            'wa' => $wa,
            'logo' => $logo,
        ];
        return view('user/home', $data);
    }

    public function blog(Request $request)
    {
        $logo = Layout::select('fill')->where('title', 'logo')->get();
        $title = Layout::select('fill')->where('title', 'name')->get();
        $address = Layout::select('fill')->where('title', 'alamat')->get();
        $tlp = Layout::select('fill')->where('title', 'tlp')->get();
        $email = Layout::select('fill')->where('title', 'email')->get();
        $header_produk = Layout::select('fill')->where('title', 'header produk')->get();
        $wa = Sosmed::select('title')->where('id', '9')->get();

        $keyword = $request->get('s') ? $request->get('s') : '';
        $category = $request->get('c') ? $request->get('c') : '';


        $articles = Article::with('categories')
            ->whereHas('categories', function ($q) use ($category) {
                $q->where('name', 'LIKE', "%$category%");
            })
            ->where('status', 'PUBLISH')
            ->where('title', 'LIKE', "%$keyword%")
            ->orderBy('created_at', 'desc')
            ->paginate(10);
        $recents = Article::select('title', 'slug')->where('status', 'PUBLISH')->orderBy('created_at', 'desc')->limit(5)->get();

        $data = [
            'articles' => $articles,
            'recents' => $recents,
            'title' => $title,
            'address' => $address,
            'tlp' => $tlp,
            'email' => $email,
            'categories' => Category::all(),
            'about' => About::all(),
            'slide' => Slide::all(),
            'online' => Online::all(),
            'sosmed' => Sosmed::all(),
            'produk' => Produk::all(),
            'header_produk' => $header_produk,
            'wa' => $wa,
            'logo' => $logo,
        ];

        return view('user/blog', $data);
    }


    public function show_article($slug)
    {
        $articles = Article::where('slug', $slug)->first();
        $recents = Article::select('title', 'slug')->where('status', 'PUBLISH')->orderBy('created_at', 'desc')->limit(5)->get();
        $data = [
            'articles' => $articles,
            'recents' => $recents
        ];
        return view('user/blog', $data);
    }

    public function destination(Request $request)
    {
        $keyword = $request->get('s') ? $request->get('s') : '';

        $destinations = Destination::where('title', 'LIKE', "%$keyword%")->orderBy('created_at', 'desc')->paginate(10);
        $other_destinations = Destination::select('title', 'slug')->where('status', 'PUBLISH')->orderBy('created_at', 'desc')->limit(5)->get();

        $logo = Layout::select('fill')->where('title', 'logo')->get();
        $title = Layout::select('fill')->where('title', 'name')->get();
        $address = Layout::select('fill')->where('title', 'alamat')->get();
        $tlp = Layout::select('fill')->where('title', 'tlp')->get();
        $email = Layout::select('fill')->where('title', 'email')->get();
        $header_produk = Layout::select('fill')->where('title', 'header produk')->get();
        $wa = Sosmed::select('title')->where('id', '9')->get();
        $artikel = Artikel::where('id', '1')->firstOrFail();

        $data = [
            'destinations' => $destinations,
            'other' => $other_destinations,
            'title' => $title,
            'address' => $address,
            'tlp' => $tlp,
            'email' => $email,
            'categories' => Category::all(),
            'about' => About::all(),
            'slide' => Slide::all(),
            'online' => Online::all(),
            'sosmed' => Sosmed::all(),
            'produk' => Produk::all(),
            'header_produk' => $header_produk,
            'wa' => $wa,
            'logo' => $logo,
            'artikel' => $artikel,


        ];

        return view('user/destination', $data);
    }

    public function show_destination($slug)
    {
        $destinations = Destination::where('slug', $slug)->firstOrFail();
        $other_destinations = Destination::select('title', 'slug')->where('status', 'PUBLISH')->orderBy('created_at', 'desc')->limit(5)->get();

        $data = [
            'destinations' => $destinations,
            'other' => $other_destinations
        ];

        return view('user/destination', $data);
    }

    public function contact()
    {
        $title = Layout::select('fill')->where('title', 'name')->get();
        $address = Layout::select('fill')->where('title', 'alamat')->get();
        $tlp = Layout::select('fill')->where('title', 'tlp')->get();
        $email = Layout::select('fill')->where('title', 'email')->get();
        $header_contact = Layout::select('fill')->where('title', 'header contact')->get();
        $logo = Layout::select('fill')->where('title', 'logo')->get();
        $data = [
            'logo' => $logo,
            'title' => $title,
            'address' => $address,
            'tlp' => $tlp,
            'email' => $email,
            'categories' => Category::all(),
            'about' => About::all(),
            'slide' => Slide::all(),
            'online' => Online::all(),
            'sosmed' => Sosmed::all(),
            'produk' => Produk::all(),
            'header_contact' => $header_contact
        ];
        return view('user/contact', $data);
    }

    public function slide()
    {

        $slide = [
            'slide' => Slide::all()
        ];

        return view('user/slide', $slide);
    }

    public function online()
    {

        $online = [
            'online' => Online::all()
        ];

        return view('user/online', $online);
    }

    public function sosmed()
    {

        $sosmed = [
            'sosmed' => Sosmed::all()
        ];

        return view('user/sosmed', $sosmed);
    }

    public function layout()
    {

        $layout = [
            'layout' => Layout::all()
        ];

        return view('user/layout', $layout);
    }

    public function show_produk($slug)
    {
        $title = Layout::select('fill')->where('title', 'name')->get();
        $address = Layout::select('fill')->where('title', 'alamat')->get();
        $tlp = Layout::select('fill')->where('title', 'tlp')->get();
        $email = Layout::select('fill')->where('title', 'email')->get();
        $wa = Sosmed::select('title')->where('id', '9')->get();
        $produk = Produk::where('id', $slug)->firstOrFail();
        $header_produk = Layout::select('fill')->where('title', 'header produk')->get();
        $logo = Layout::select('fill')->where('title', 'logo')->get();
        $data = [
            'produk' => $produk,
            'title' => $title,
            'address' => $address,
            'tlp' => $tlp,
            'email' => $email,
            'wa' => $wa,
            'logo' => $logo,
            'header_produk' => $header_produk
        ];

        return view('user/produk_detail', $data);
    }

    public function show_artikel($slug)
    {
        $title = Layout::select('fill')->where('title', 'name')->get();
        $address = Layout::select('fill')->where('title', 'alamat')->get();
        $tlp = Layout::select('fill')->where('title', 'tlp')->get();
        $email = Layout::select('fill')->where('title', 'email')->get();
        $wa = Sosmed::select('title')->where('id', '9')->get();
        $produk = Produk::where('id', $slug)->firstOrFail();
        $artikel = Artikel::where('id', $slug)->firstOrFail();
        $header_produk = Layout::select('fill')->where('title', 'header produk')->get();
        $logo = Layout::select('fill')->where('title', 'logo')->get();
        $data = [
            'produk' => $produk,
            'title' => $title,
            'address' => $address,
            'tlp' => $tlp,
            'email' => $email,
            'wa' => $wa,
            'header_produk' => $header_produk,
            'artikel' => $artikel,
            'logo' => $logo,
        ];
        return view('user/artikel_detail', $data);
    }
}
