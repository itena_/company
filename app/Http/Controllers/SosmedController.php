<?php

namespace App\Http\Controllers;
use App\Sosmed;
use Illuminate\Http\Request;
use File;

class SosmedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $status     = $request->get('status');
        $keyword    = $request->get('keyword') ? $request->get('keyword') : '';
        $category   = $request->get('c') ? $request->get('c') : '';

        if($status){
            $sosmed = Sosmed::where('status', strtoupper($status))
                ->paginate(10);

        }else{
            $sosmed = Sosmed::where('title', 'LIKE', "%$keyword%")
                ->paginate(10);
        }

        return view('sosmed.index', compact('sosmed'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('sosmed.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        \Validator::make($request->all(),[
            'title'      => 'required|min:2|max:200',
            'image'      => 'required',
        ])->validate();

        $new_Sosmed               = new \App\Sosmed();
        $new_Sosmed->title        = $request->get('title');

        if($request->file('image')){
            $nama_file = time()."_".$request->file('image')->getClientOriginalName();
            $image_path = $request->file('image')->move('sosmed_image', $nama_file);
            $new_Sosmed->image = $nama_file;
        }
        $new_Sosmed->save();

        return redirect()->route('sosmed.index')->with('success', 'Sosmed successfully created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sosmed = Sosmed::findOrFail($id);
        return view('sosmed.edit', compact('sosmed'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $sosmed = Sosmed::findOrFail($id);

        $sosmed->title        = $request->get('title');

        if($request->file('image')){
            if($sosmed->image){
                File::delete('sosmed_image/'.$sosmed->image);
            }
            $nama_file = time()."_".$request->file('image')->getClientOriginalName();
            $new_image = $request->file('image')->move('sosmed_image', $nama_file);
            $sosmed->image = $nama_file;
        }

        $sosmed->save();
        return redirect()->route('sosmed.index')->with('success', 'Sosmed successfully update.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sosmed = Sosmed::findOrFail($id);
        if($sosmed->image){
            File::delete('sosmed_image/'.$sosmed->image);
        }
        $sosmed->forceDelete();

        return redirect()->route('sosmed.index')->with('success', 'Sosmed successfully deleted.');
    }
}

