<?php

namespace App\Http\Controllers;
use App\Slide;
use App\About;
use Illuminate\Http\Request;
use File;

class SlideController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $status     = $request->get('status');
        $keyword    = $request->get('keyword') ? $request->get('keyword') : '';
        $category   = $request->get('c') ? $request->get('c') : '';

        if($status){
            $slide = Slide::where('status', strtoupper($status))
                ->paginate(10);

        }else{
            $slide = Slide::where('title', 'LIKE', "%$keyword%")
                ->paginate(10);
        }

        return view('slide.index', compact('slide'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('slide.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        \Validator::make($request->all(),[
            'title'      => 'required|min:2|max:200',
            'image'      => 'required',
        ])->validate();

        $new_Slide               = new \App\Slide();
        $new_Slide->title        = $request->get('title');

        if($request->file('image')){
            $nama_file = time()."_".$request->file('image')->getClientOriginalName();
            $image_path = $request->file('image')->move('slide_image', $nama_file);
            $new_Slide->image = $nama_file;
        }


        $new_Slide->save();

        return redirect()->route('slide.index')->with('success', 'Slide successfully created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slide = Slide::findOrFail($id);
        return view('slide.edit', compact('slide'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $slide = Slide::findOrFail($id);

        $slide->title        = $request->get('title');

        if($request->file('image')){
            if($slide->image){
                File::delete('slide_image/'.$slide->image);
            }
            $nama_file = time()."_".$request->file('image')->getClientOriginalName();
            $new_image = $request->file('image')->move('slide_image', $nama_file);
            $slide->image = $nama_file;
        }

        $slide->save();
        return redirect()->route('slide.index')->with('success', 'Slide successfully update.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slide = Slide::findOrFail($id);
        if($slide->image){
            File::delete('slide_image/'.$slide->image);
        }
        $slide->forceDelete();

        return redirect()->route('slide.index')->with('success', 'Slide successfully deleted.');
    }
}

