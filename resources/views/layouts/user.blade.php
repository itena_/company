<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    @foreach($title as $key => $val)
        <title>{{$val['fill']}}</title>
    @endforeach

    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

    <!-- Favicons -->
    <link href="{{asset('user/images/favicon.png')}}" rel="icon">
    <link href="{{asset('user/images/apple-touch-icon.png')}}" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Poppins:300,400,500,700"
          rel="stylesheet">

    <!-- Bootstrap CSS File -->
    <link href="{{asset('user/lib/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Libraries CSS Files -->
    <link href="{{asset('user/lib/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('user/lib/animate/animate.min.css')}}" rel="stylesheet">

    <!-- Main Stylesheet File -->
    <link href="{{asset('user/css/style.css')}}" rel="stylesheet">

    @yield('header')


</head>

<body>

@php
    $url = request()->segment(1);
@endphp

<!--========================== Header ============================-->
<header id="header">
    <div class="container">
        <div class="pull-left text-light">
            @foreach($logo as $key => $val)
                <p><img src="{{asset('layout_image/'.$val['fill'])}}" style="width:75px;"/></img></p>
            @endforeach
        </div>
        <nav id="nav-menu-container">
            <ul class="nav-menu">
                <li class="{{$url=='home'?'menu-active':''}}"><a style="font-family: Bahnschrift"
                                                                 href="{{url('home')}}">HOME</a></li>
                <li><a style="font-family: Bahnschrift" href="{{url('home#about')}}">ABOUT US</a></li>
                <li><a style="font-family: Bahnschrift" href="{{url('home#section_product')}}">PRODUCT</a></li>
                <li><a style="font-family: Bahnschrift" href="{{url('home#section_artikel')}}">ARTICLE</a></li>
                <li><a style="font-family: Bahnschrift" href="{{url('home#section_contact')}}">CONTACT US</a></li>

{{--                <li class="{{$url=='destination'?'menu-active':''}}"><a href="{{url('destination')}}">Destination</a></li>--}}
{{--                <li class="{{$url=='contact'?'menu-active':''}}"><a href="{{url('contact')}}">Contact Us </a></li>--}}
                {{--                <li class="{{$url=='blog'?'menu-active':''}}"><a href="{{url('blog')}}">Product Page</a></li>--}}
            </ul>
        </nav>
    </div>
</header><!-- #header -->

<!--========================== Hero Section ============================-->
<section id="hero">
    <div class="hero-container">
        @yield('hero')
    </div>
</section>

<main id="main">

    @yield('content')

</main>

<!--==========================
  Footer
============================-->
<footer id="footer">
    <p style="text-align: center;font-size: 10px">Copyright © 2021 || Designed by IT Department</p>
</footer><!-- #footer -->

<a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

<!-- JavaScript Libraries -->
<script src="{{asset('user/lib/jquery/jquery.min.js')}}"></script>
<script src="{{asset('user/lib/easing/easing.min.js')}}"></script>
<script src="{{asset('user/lib/wow/wow.min.js')}}"></script>
<script src="{{asset('user/lib/superfish/superfish.min.js')}}"></script>
<script src="{{asset('user/js/main.js')}}"></script>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<a href="@foreach($wa as $key => $val){{$val['title']}}@endforeach"
   style="position:fixed;
	width:60px;
	height:60px;
	bottom:40px;
	right:40px;
	background-color:#25d366;
	color:#FFF;
	border-radius:50px;
	text-align:center;
  font-size:30px;
	box-shadow: 2px 2px 3px #999;
  z-index:100;" target="_blank">
    <i class="fa fa-whatsapp" style="margin-top:16px;"></i>
</a>

</body>
</html>
