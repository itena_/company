@extends('layouts.admin')

@section('title', 'Edit Layout')

@section('breadcrumbs', 'Layout')

@section('second-breadcrumb')
    <li> Edit Layout</li>
@endsection

@section('content')
    <!-- table  -->
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">

                        @if (session('success'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {{session('success')}}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif

                        <h3 class="text-center mt-3 mb-5">Layout</h3>

                        <div class="row">
                            <div class="col-12">
                                <form action="{{route('layout.update', [$layout->id])}}" method="POST" class="d-inline" enctype="multipart/form-data">
                                    @csrf
                                    @method('PUT')
                                    <p>Edit {{$layout->title}}<br>
                                    </p>
                                    @if($layout->title == "logo" || $layout->title == "header produk")
                                    <div class="row">
                                        <input name="title" id="content" rows="10" class="form-control" value="{{$layout->title}}" hidden>
                                        <img src="{{asset('layout_image/'.$layout->fill)}}" alt="fill" width="500px">
                                        <div class="d-inline col-6" >
                                            <input type="file" name="fill" style="padding-top: 5px;padding-bottom: 5px"><br>
                                            @if($layout->title == "logo")
                                                <p>Image Size 75 x 75 pixel</p>
                                            @else
                                                <p>Image 1900 x 200 px</p>
                                            @endif
                                            <button type="submit" class="btn btn-success top-0"> Update</button>
                                        </div>
                                    </div>
                                    @else
                                        <input name="title" id="content" rows="10" class="form-control" value="{{$layout->title}}" hidden>
                                        <p>
                                            <input name="fill" id="content" rows="10" class="form-control" value="{{$layout->fill}}" >
                                        </p>
                                        <div class="d-inline col-6" >
                                            <button type="submit" class="btn btn-success top-0"> Update</button>
                                        </div>
                                @endif
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    <!-- /table -->
@endsection

