@extends('layouts.admin')

@section('title', 'Layout')

@section('breadcrumbs', 'Overview Layout')

@section('css')
    <style>
        .underline:hover {
            text-decoration: underline;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    @if (session('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{session('success')}}.
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        </div>
                    @endif

                    {{-- table --}}
                    <table class="table">
                        <thead class="text-light" style="background-color:#33b751 !important">
                        <tr>
                            <th>Layout</th>
                            <th>Value</th>
                            <th></th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($layout as $index => $layout)
                            <tr>
                                <td align="left">
                                    <a style="color:#00838f;" class="underline" target="_blank">
                                        <span class="d-block">{{$layout->title}}</span>
                                    </a>
                                </td>
                                @if($layout->title == "logo" || $layout->title == "header produk")
                                    <td align="left">
                                        <img src="{{asset('layout_image/'.$layout->fill)}}" alt="" width="100px">
                                    </td>
                                @else
                                    <td>
                                        <a style="color:#00838f;" class="underline" target="_blank">
                                            <span class="d-block">{{$layout->fill}}</span>
                                        </a>
                                    </td>
                                @endif

                                <td>
                                    <a href="{{route('layout.edit', [$layout->id])}}"
                                       class="bnt btn-sm btn-warning text-light" title="Edit"><i
                                            class="fa fa-pencil"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>
                </div>

            </div>
        </div>
    </div>


    <!-- Modal Delete -->
    <div class="modal fade" id="modalDelete" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title d-inline">Delete Social Media</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="message">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <form action="" id="url" method="POST" class="d-inline">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal Delete -->


@endsection

@section('script')
    <script>
        function deleteConfirm(id, name) {
            var url = '{{ route("layout.destroy", ":id") }}';
            url = url.replace(':id', id);
            document.getElementById("url").setAttribute("action", url);
            document.getElementById('message').innerHTML = "Are you sure want to delete Social Media <b>" + name + "</b> ?"
            $('#modalDelete').modal();
        }

    </script>
@endsection

