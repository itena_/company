@extends('layouts.admin')

@section('title', 'Social Media')

@section('breadcrumbs', 'Overview Social Media')

@section('css')
    <style>
        .underline:hover{
            text-decoration: underline;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">

                    {{-- button create --}}
                    <div class="mb-5 text-right">
                        <a href="{{route('sosmed.create')}}" class="btn btn-sm btn-success"> <i class="fa fa-plus"></i> Create</a>
                    </div>
                    {{-- alert --}}
                    @if (session('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{session('success')}}.
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                    @endif

                    {{-- table --}}
                    <table class="table">
                        <thead class="text-light" style="background-color:#33b751 !important">
                        <tr>
                            {{-- <th width="12px">No</th> --}}
                            <th width="160px">Image</th>
                            <th class="">Social Media Address</th>
                            <th width="150px"></th>
                            <th width="88px">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($sosmed as $index => $sosmed)

                            <tr>
                                {{-- <td>{{$index+1}}</td> --}}
                                <td align="left">
                                    @if($sosmed->image)
                                        <img src="{{asset('sosmed_image/'.$sosmed->image)}}" alt="" width="40px">
                                    @endif
                                </td>
                                <td>
                                    <a href="{{$sosmed->title}}" style="color:#00838f;" class="underline" target="_blank">
                                        <span class="d-block">{{$sosmed->title}}</span>
                                    </a>
                                </td>
                                <td class="text-right pr-4">
                                    @if ($sosmed->status=='DRAFT')
                                        <span class="font-italic text-danger">Draft</span>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{route('sosmed.edit', [$sosmed->id])}}" class="bnt btn-sm btn-warning text-light" title="Edit"><i class="fa fa-pencil"></i></a>
                                    <button class="btn btn-sm btn-danger" onclick="deleteConfirm('{{$sosmed->id}}', '{{$sosmed->title}}')" data-target="#modalDelete" data-toggle="modal"><i class="fa fa-trash"></i></button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
{{--                        {{$slides->appends(Request::all())->links()}}--}}
                        </tfoot>
                    </table>
                </div>

            </div>
        </div>
    </div>


    <!-- Modal Delete -->
    <div class="modal fade" id="modalDelete" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title d-inline">Delete Social Media</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="message">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <form action="" id="url" method="POST" class="d-inline">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal Delete -->


@endsection

@section('script')
    <script>
        function deleteConfirm(id, name){
            var url = '{{ route("sosmed.destroy", ":id") }}';
            url = url.replace(':id', id);
            document.getElementById("url").setAttribute("action", url);
            document.getElementById('message').innerHTML ="Are you sure want to delete Social Media <b>"+name+"</b> ?"
            $('#modalDelete').modal();
        }

    </script>
@endsection

