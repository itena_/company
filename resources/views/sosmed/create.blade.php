@extends('layouts.admin')

@section('title', 'Create Sosmed')

@section('breadcrumbs', 'Sosmed' )

@section('second-breadcrumb')
    <li>Create</li>
@endsection

@section('css')
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="col-12 mb-3">
                        <h3 align="center"></h3>
                    </div>
                    <form action="{{route('sosmed.store')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="col-10">
                            <div class="mb-4">
                                <label for="title" class="font-weight-bold">Social Media Address</label>
                                <input type="text" name="title" placeholder="Social Media Address..."
                                       class="form-control {{$errors->first('title') ? "is-invalid" : ""}}"
                                       value="{{old('title')}}" required>
                                <div class="invalid-feedback"> {{$errors->first('title')}}</div>
                            </div>
                            <div class="mb-3">
                                <label for="slug" class="font-weight-bold">Image 200 x 200 pixel</label>
                                <input type="file" name="image"
                                       class="form-control {{$errors->first('image') ? "is-invalid" : ""}}" required>
                                <div class="invalid-feedback"> {{$errors->first('image')}}</div>
                            </div>
                            <div class="mb-3 mt-4">
                                <button class="btn btn-success" name="save_action" value="PUBLISH">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

