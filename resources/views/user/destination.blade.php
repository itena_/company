@extends('layouts.user')

@section('header')
    <style>
        #hero {
            width: 100%;
            height: 0vh;
            background-size: cover;
            background-color: powderblue;
        }
    </style>
@endsection

@section('hero')
@endsection

@section('content')
    <section id="about" style="padding-top: 80px;">
        <hr>
        <div class="container col-md-24" style="background-color: beige">
            <div class="col-md-6">
                @if($artikel['image'])
                    <img style="text-align: center;padding: 20px"
                         src="{{asset('artikel_image/'.$artikel['image'])}}" width="500px">
                @endif
            </div>
            <div style="text-align: left;padding-top: 30px;font-family: Consolas; font-size: 28px">
                <b>{{$artikel['title']}}</b></div>
            <p style="text-align: justify;font-size: 12px">{{$artikel['description']}}</p>
        </div>
        <hr>
    </section>
@endsection
