@extends('layouts.user')

@section('header')
    <style>
        #hero {
{{--            @foreach($header_produk as $key => $val)--}}
{{--              background: url('{{asset('layout_image/'.$val['fill'])}}') top center;--}}
{{--            background-repeat: no-repeat;--}}
{{--            width: 100%;--}}
            height: 0vh;
            /*height: 100vh;*/
            /*background-size: cover;*/
            /*background-color: powderblue;*/
{{--        @endforeach--}}
}

    </style>
@endsection

@section('hero')
@endsection


@section('content')
    <div class="col-md-24">
        @foreach($header_produk as $key => $val)
            <div id="produk_header" style="background: url('{{asset('layout_image/'.$val['fill'])}}') top center;background-repeat: no-repeat;">
            </div>
        @endforeach
    </div>
    <!--========================== PRODUCT Section ============================-->
    <section id="section_product" >
        <div style="text-align: center;">
            <div class="" style="padding-bottom: 50px;">
                <div id="myCarousel" class="row wow pulse" style="padding-left: 70px;padding-top: 50px">
                    <?php $count = 0; ?>
                    @foreach ($produk as  $produk)
                        <div
                            style="margin: 25px;width: 210px;border: 2px solid whitesmoke;align-content: center;text-align: center;border-radius: 10px">
                            <a><img style="text-align: center;padding-top: 5px"
                                    src="{{asset('produk_image/'.$produk->image)}}" alt="" width="200px" height="200px"></a>
                            <br>
                            <hr>
                            <a href="{{route('user.show_produk', [$produk->id])}}"
                               style="font-size: 14px;text-align: left;font-family: 'Consolas';color: black">
                                <span style="margin-left:10px" class="d-block">{{$produk->title}}</span>
                            </a>
                            <a style="font-size: 15px;text-align: left;font-family: 'fantasy'">
                                <span style="margin-left:10px"
                                      class="d-block">Rp. {{number_format($produk->price,2, ".", ",")}}</span><br>
                            </a>
                        </div>

                        <?php $count++; ?>
                    @endforeach
                    <div>

                    </div>
                </div>
    </section>
@endsection
