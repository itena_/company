@extends('layouts.user')

@section('header')
    <style>
        #hero {
            @foreach($header_produk as $key => $val)
                    background: url('{{asset('user/images/'.$val['fill'])}}') top center;
            background-repeat: no-repeat;
            width: 100%;
            height: 35vh;
            background-size: cover;
            background-color: powderblue;
        @endforeach


        }
    </style>
@endsection

@section('hero')
@endsection

@section('content')
    <section id="about" style="padding-top: 10px">
        <div class="container col-md-24">
            <div class="col-md-6">
                @if($artikel['image'])
                    <img style="text-align: center;padding: 20px"
                         src="{{asset('artikel_image/'.$artikel['image'])}}" width="400px">
                @endif
            </div>
            <div style="text-align: left;padding-top: 30px;font-family: Consolas; font-size: 28px">
                <b>{{$artikel['title']}}</b></div>
            <p style="text-align: justify;font-size: 12px">{{$artikel['description']}}</p>
        </div>
        <hr>
    </section>
@endsection
