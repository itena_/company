@extends('layouts.user')

@section('header')
    <style>
        #hero {
            @foreach($header_produk as $key => $val)
                    background: url('{{asset('layout_image/'.$val['fill'])}}') top center;
            background-repeat: no-repeat;
            width: 100%;
            height: 35vh;
            background-size: cover;
            background-color: powderblue;
        @endforeach


        }
    </style>
@endsection

@section('hero')
@endsection

@section('content')
    <section id="about" style="padding-top: 10px">
        <div class="container col-md-24">
            <div class="col-md-6">
                @if($produk['image'])
                    <img style="text-align: center;padding: 20px"
                         src="{{asset('produk_image/'.$produk['image'])}}" width="400px">
                @endif
            </div>
            <div style="text-align: left;padding-top: 30px;font-family: Consolas; font-size: 28px">
                <b>{{$produk['title']}}</b></div>
            <div style="text-align: left;font-size: 20px;padding-top: 10px"> Rp. {{$produk['price']}}</div>
            <p style="text-align: justify;font-size: 18px;padding-top: 10px">Weight | {{$produk['weight']}}</p>
            <p style="text-align: justify;font-size: 12px">{{$produk['description']}}</p>
            <p style="text-align: justify;font-size: 15px"> Order Now : &nbsp
            <a href="@foreach($wa as $key => $val){{$val['title']}}@endforeach"> <i class="fa fa-whatsapp"></i>
             +{{trim($val['title'], 'https://wa.me/')}} </a></p>
        </div>
        <hr>
    </section>
@endsection
