@extends('layouts.user')
@section('header')
@endsection

@section('hero')
    <!--========================== SLIDE Section ============================-->
    <div class="col-md-24">
        <section id="hero">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    @forelse($slide as $index => $slides)
                        @if ($loop->first)
                            <div class="item active">
                                <img src="{{asset('slide_image/'.$slides->image)}}">
                            </div>
                        @else
                            <div class="item">
                                <img src="{{asset('slide_image/'.$slides->image)}}">
                            </div>
                        @endif
                    @empty
                        <div class="col-xs-12">
                            <h3 class="text-center">No images uploaded</h3>
                        </div>
                    @endforelse
                </div>
            </div>
        </section>
    </div>

@endsection


@section('content')
    <!--========================== ABOUT US Section ============================-->
    <div style="background-color: white;padding-top: 150px">
        <div id="about" class="container" style="align-content: center;">
            <div class="col-md-12 wow fadeInLeft" style="margin-bottom: 30px;">
                <div class="col-lg-6">
                    <h1 style="font-family: Consolas;font-size: 32px;padding-top: 20px">{{$about[0]->title}}</h1>
                    <p style="text-align: justify;font-size: small"> {{$about[0]->caption}}</p>
                </div>
                <div class="col-lg-6" style="border: 2px black;">
                    <img class="pull-left" src="{{asset('about_image/'.$about[0]->image)}}"
                         style="width: 550px;height: 350px;">
                </div>
            </div>
            <div class="col-md-12 wow fadeInRight" style="padding-top: 20px">
                <div class="col-lg-6" style="border: 2px black;">
                    <img src="{{asset('about_image/'.$about[1]->image)}}"
                         style="width: 550px;height: 350px;border-radius: 5px;">
                </div>

                <div class="col-lg-6">
                    <h1 style="padding-left:10px;font-family: Consolas;font-size: 22px;padding-top: 20px">{{$about[1]->title}}</h1>
                    <p style="padding-left:10px;text-align: justify;font-size: small;"> {{$about[1]->caption}}</p>
                </div>

                <div class="col-lg-6">
                    <h1 style="padding-left:10px;font-family: Consolas;font-size: 22px;padding-top: 20px">{{$about[2]->title}}</h1>
                    <p style="padding-left:10px;text-align: justify;font-size: small;"> {{$about[2]->caption}}</p>
                </div>
            </div>
        </div>
    </div>


    <!--========================== PRODUCT Section ============================-->
    <section id="section_product" class="wow fadeInUp"
             style="background: linear-gradient(to top, #ffffff 80%, gainsboro 100%); ">
        <div style="text-align: center;">
            <br><br>
            <h1 style="padding-top: 10px">OUR PRODUCT</h1>

            <div class="container" style="padding-bottom: 50px;">
                <div id="myCarousel" class="row wow pulse" style="padding-left: 100px;">
                    <?php $count = 0; ?>
                    @foreach ($produk as  $produk)
                        <?php if ($count == 4) break;?>
                        <div
                            style="box-shadow:3px 3px 1px whitesmoke;margin: 20px;width: 210px;border: 2px solid whitesmoke;align-content: center;text-align: center;border-radius: 10px">
                            <a><img style="text-align: center;padding-top: 5px"
                                    src="{{asset('produk_image/'.$produk->image)}}" alt="" width="200px" height="200px"></a>
                            <br>
                            <hr>
                            <a href="{{route('user.show_produk', [$produk->id])}}"
                               style="font-size: 14px;text-align: left;font-family: 'Consolas';color: black">
                                <span style="margin-left:10px" class="d-block">{{$produk->title}}</span>
                            </a>
                            <a style="font-size: 15px;text-align: left;font-family: 'fantasy'">
                                <span style="margin-left:10px"
                                      class="d-block">Rp. {{number_format($produk->price,2, ".", ",")}}</span><br>
                            </a>
                        </div>

                        <?php $count++; ?>
                    @endforeach
                    <div>

                    </div>
                </div>
                <div>
                    <br><br>
                    <a style=" background-color: cornflowerblue;color: white;padding: 14px 25px;text-align: center;text-decoration: none;display: inline-block;font-size: 14px"
                       href="{{url('blog')}}"> MORE PRODUCT</a>
                </div>
    </section>





    <!--========================== PRODUCT Artikel ============================-->
    <section id="section_artikel" class="wow fadeInUp"
             style=" text-align: center;background-color: whitesmoke">
        <div style="text-align: center;">
            <h1 style="padding-top: 20px">TIPS AND ARTICLE</h1><br>
            <div class="container" style="padding-bottom: 50px">
                <div class="row wow pulse">
                    <?php $count = 0; ?>
                    @foreach ($artikel as  $artikel)
                        <?php if ($count == 3) break;?>
                        <div
                            style="margin: 15px;width: 350px;border: 2px solid whitesmoke;">
                            @if($artikel->image)
                                <a><img style="text-align: center;"
                                        src="{{asset('artikel_image/'.$artikel->image)}}" alt="" width="290px"></a>
                                <hr>
                            @endif
                                <a href="{{route('destination', [$artikel->id])}}"
                               style="font-size: 20px;text-align: center;font-family: 'Consolas'">
                                <span class="d-block">{{$artikel->title}}</span>
                            </a><br>
                        </div>
                        <?php $count++; ?>
                    @endforeach
                </div>
    </section>

    <!--========================== CONTACT Section ============================-->
    <section id="section_contact" class="wow fadeInUp" style="padding-top: 10px;">
        <div class="container">
            <div class="row col-md-12" style="padding-top: 5px">
                <div class="col-md-4" style="padding: 5px">
                    <div class="col-md-2"><i class="fa fa-map-marker"
                                             style="font-size: 5em;color: black;margin-top: 10px;"></i></div>
                    <div class="col-md-10">
                        <a style="font-size:18px;font-family: 'Arial Narrow'; ">OFFICE :</a><br>
                        <a style="font-size:14px;font-family: 'Arial Narrow'; ">@foreach($address as $key => $val){{$val['fill']}}@endforeach</a>
                    </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-3" style="padding: 5px">
                    <div class="col-md-3"><i class="fa fa-phone"
                                             style="font-size: 5em;color: black;margin-top: 10px;"></i>
                    </div>
                    <div class="col-md-9">
                        <a style="font-size:18px;font-family: 'Arial Narrow'; ">PHONE :</a><br>
                        <a style="font-size:14px;font-family: 'Arial Narrow'; ">Tlp
                            : @foreach($tlp as $key => $val){{$val['fill']}}@endforeach</a><br>
                        <a style="font-size:12px;font-family: 'Arial Narrow'; ">WhatsApp
                            : @foreach($tlp as $key => $val){{$val['fill']}}@endforeach</a>
                    </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-3" style="padding: 5px">
                    <div class="col-md-3"><i class="fa fa-envelope"
                                             style="font-size: 5em;color: black;margin-top: 5px;"></i></div>
                    <div class="col-md-8">
                        <a style="font-size:18px;font-family: 'Arial Narrow'; ">E-MAIL :</a><br>
                        <a style="font-size:14px;font-family: 'Arial Narrow'; ">@foreach($email as $key => $val){{$val['fill']}}@endforeach</a>
                    </div>
                </div>

            </div>
            <div class="col-md-12" style="padding-top: 50px;">
                <div class="col-md-6" style="margin-top: 20px;text-align: center;padding-bottom: 20px">
                    <h2 style="padding-top: 10px;text-align: center;font-family: 'Franklin Gothic';">FOLLOW US ON SOCIAL
                        MEDIA</h2>
                    @foreach($sosmed as $index => $sosmed)
                        <a href="{{$sosmed->title}}" target="_blank"><img
                                src="{{asset('sosmed_image/'.$sosmed->image)}}" style="padding: 20px;width: 90px"></a>
                    @endforeach
                </div>
                <div class="col-md-6" style="margin-top: 20px;text-align: center;padding-bottom: 20px">
                    <h2 style="padding-top: 10px;text-align: center;font-family: 'Franklin Gothic';">FIND THE
                        PRODUCT</h2>
                    @forelse($online as $index => $online)
                        <a href="{{$online->title}}" target="_blank"><img
                                src="{{asset('online_image/'.$online->image)}}"
                                style="padding: 20px;width: 150px"></a>
                    @empty
                        <div class="col-xs-12">
                            <h3 class="text-center">No Online Shope uploaded</h3>
                        </div>
                    @endforelse
                </div>
            </div>

        </div>

    </section>
@endsection

