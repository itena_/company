@extends('layouts.admin')

@section('title', 'Edit Product')

@section('breadcrumbs', 'Product')

@section('second-breadcrumb')
    <li> Edit Product</li>
@endsection

@section('content')
    <!-- table  -->
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">

                        @if (session('success'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {{session('success')}}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>

                        @endif

                        <h3 class="text-center mt-3 mb-5">Edit Product</h3>

                        <div class="row">
                            <div class="col-3 mt-4">
                                <div class="card shadow" >
                                    <img src="{{asset('produk_image/'.$produk->image)}}" class="card-img-top" alt="image">
                                </div>
                            </div>
                            <div class="col-9">
                                <form action="{{route('produk.update', [$produk->id])}}" method="POST" class="d-inline" enctype="multipart/form-data">
                                    @csrf
                                    @method('PUT')
                                    <p>Product</p>
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label for="title" class="font-weight-bold">Category</label>
                                            <select name="kategori" id="kategori" class="form-control {{$errors->first('kategori') ? "is-invalid" : ""}}"  value="{{old('kategori')}}">
                                                <option value="Facial Wash">Facial Wash</option>
                                                <option value="Serum">Serum</option>
                                                <option value="Sunscreen">Sunscreen</option>
                                                <option value="Toner">Toner</option>
                                            </select>
                                            {{--                                <input type="text" name="title" placeholder="Product Category..." class="form-control {{$errors->first('kategori') ? "is-invalid" : ""}}" value="{{old('title')}}" required>--}}
                                            <div class="invalid-feedback"> {{$errors->first('kategori')}}</div>
                                        </div>
                                        <div class="mb-1">
                                            <label for="title" class="font-weight-bold">Name</label>
                                            <input type="text" name="title" class="form-control {{$errors->first('title') ? "is-invalid" : ""}}" value="{{$produk->title}}">
                                            <div class="invalid-feedback"> {{$errors->first('title')}}</div>
                                        </div>
                                        <div class="mb-1">
                                            <label for="title" class="font-weight-bold">Description</label>
                                            <textarea type="text" name="description" class="form-control"> {{$produk->description}} </textarea>
                                            <div class="invalid-feedback"> {{$errors->first('description')}}</div>
                                        </div>
                                        <div class="mb-1">
                                            <label for="title" class="font-weight-bold">Weight</label>
                                            <input type="text" name="weight" placeholder="100 gr" class="form-control" value="{{$produk->weight}}">

                                        </div>
                                        <div class="mb-1" >
                                            <label for="title" class="font-weight-bold">Price</label>
                                            <input type="number" name="price" placeholder="10000" class="form-control" value="{{$produk->price}}" >
                                            <div class="invalid-feedback"> {{$errors->first('price')}}</div>
                                        </div>
                                        <label for="title" class="font-weight-bold">Image</label>
                                        <div class="row">
                                            <div class="d-inline col-6" >
                                                <input type="file" name="image">
                                                <br>
                                                <small class="font-italic text-danger" style="font-size:11px; ">Leave blank if you don't want to change your avatar</small>
                                            </div>
                                            <div class="col-5 d-inline">
                                                <button type="submit" class="btn btn-success top-0"> Update</button>
                                            </div>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    <!-- /table -->
@endsection

@section('script')
@endsection
