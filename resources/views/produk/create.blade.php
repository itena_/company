@extends('layouts.admin')

@section('title', 'Create Produk')

@section('breadcrumbs', 'Produk' )

@section('second-breadcrumb')
    <li>Create</li>
@endsection

@section('css')
    <script src="/templateEditor/ckeditor/ckeditor.js"></script>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="col-12 mb-3">
                        <h3 align="center"></h3>
                    </div>
                    <form action="{{route('produk.store')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="col-12">
                            <div class="mb-1">
                                <label for="title" class="font-weight-bold">Category</label>
                                <select name="kategori" id="kategori" class="form-control {{$errors->first('kategori') ? "is-invalid" : ""}}"  value="{{old('kategori')}}">
                                    <option value="Facial Wash">Facial Wash</option>
                                    <option value="Serum">Serum</option>
                                    <option value="Sunscreen">Sunscreen</option>
                                    <option value="Toner">Toner</option>
                                </select>
                                <div class="invalid-feedback"> {{$errors->first('kategori')}}</div>
                            </div>
                            <div class="mb-1">
                                <label for="title" class="font-weight-bold">Name</label>
                                <input type="text" name="title" placeholder="Product Name..." class="form-control {{$errors->first('title') ? "is-invalid" : ""}}" value="{{old('title')}}" required>
                                <div class="invalid-feedback"> {{$errors->first('title')}}</div>
                            </div>
                            <div class="mb-1">
                                <label for="title" class="font-weight-bold">Description</label>
                                <textarea type="text" name="description" placeholder="Product Description..." class="form-control {{$errors->first('description') ? "is-invalid" : ""}}" value="{{old('description')}}" required></textarea>
                                <div class="invalid-feedback"> {{$errors->first('description')}}</div>
                            </div>
                            <div class="mb-1">
                                <label for="title" class="font-weight-bold">Weight</label>
                                <input type="text" name="weight" placeholder="100 gr" class="form-control {{$errors->first('weight') ? "is-invalid" : ""}}" value="{{old('weight')}}" required>

                            </div>
                            <div class="mb-1" >
                                <label for="title" class="font-weight-bold">Price</label>
                                <input type="number" name="price" placeholder="10000" class="form-control {{$errors->first('price') ? "is-invalid" : ""}}" value="{{old('price')}}" required>
                                <div class="invalid-feedback"> {{$errors->first('price')}}</div>
                            </div>
                            <div class="mb-1">
                                <label for="slug" class="font-weight-bold">Image 300 x 300 pixel</label>
                                <input type="file" name="image" class="form-control {{$errors->first('image') ? "is-invalid" : ""}}" required>
                                <div class="invalid-feedback"> {{$errors->first('image')}}</div>
                            </div>
                            <div class="mb-3 mt-4">
{{--                                <button class="btn btn-secondary" name="save_action" value="DRAFT">Save as draft</button>--}}
                                <button class="btn btn-success" name="save_action" value="PUBLISH">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

