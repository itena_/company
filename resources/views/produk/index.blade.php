@extends('layouts.admin')

@section('title', 'Product')

@section('breadcrumbs', 'Overview Product')

@section('css')
    <style>
        .underline:hover{
            text-decoration: underline;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">

                    {{-- button create --}}
                    <div class="mb-5 text-right">
                        <a href="{{route('produk.create')}}" class="btn btn-sm btn-success"> <i class="fa fa-plus"></i> Create</a>
                    </div>
                    {{-- alert --}}
                    @if (session('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{session('success')}}.
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                    @endif

                    {{-- table --}}
                    <table class="table">
                        <thead class="text-light" style="background-color:#33b751 !important">
                        <tr>
                            {{-- <th width="12px">No</th> --}}
                            <th class="">Kategori</th>
                            <th width="160px">Image</th>
                            <th class="">Name</th>
                            <th class="">Description</th>
                            <th class="">Weight</th>
                            <th class="">Price</th>
                            <th width="150px"></th>
                            <th width="88px">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($produk as $index => $produk)

                            <tr>
                                {{-- <td>{{$index+1}}</td> --}}
                                <td>
                                    <a style="color:#00838f;" class="underline" target="_blank">
                                        <span class="d-block">{{$produk->kategori}}</span>
                                    </a>
                                </td>
                                <td align="left">
                                    @if($produk->image)
                                        <img src="{{asset('produk_image/'.$produk->image)}}" alt="" width="80px">
                                    @endif
                                </td>
                                <td>
                                    <a style="color:#00838f;" class="underline" target="_blank">
                                        <span class="d-block">{{$produk->title}}</span>
                                    </a>
                                </td>
                                <td>
                                    <a  style="color:#00838f;" class="underline" target="_blank">
                                        <span class="d-block">{{$produk->description}}</span>
                                    </a>
                                </td>
                                <td>
                                    <a style="color:#00838f;" class="underline" target="_blank">
                                        <span class="d-block">{{$produk->weight}}</span>
                                    </a>
                                </td>
                                <td>
                                    <a style="color:#00838f;" class="underline" target="_blank">
                                        <span class="d-block">{{$produk->price}}</span>
                                    </a>
                                </td>
                                <td class="text-right pr-4">
                                    @if ($produk->status=='DRAFT')
                                        <span class="font-italic text-danger">Draft</span>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{route('produk.edit', [$produk->id])}}" class="bnt btn-sm btn-warning text-light" title="Edit"><i class="fa fa-pencil"></i></a>
                                    <button class="btn btn-sm btn-danger" onclick="deleteConfirm('{{$produk->id}}', '{{$produk->title}}')" data-target="#modalDelete" data-toggle="modal"><i class="fa fa-trash"></i></button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
{{--                        {{$slides->appends(Request::all())->links()}}--}}
                        </tfoot>
                    </table>
                </div>

            </div>
        </div>
    </div>


    <!-- Modal Delete -->
    <div class="modal fade" id="modalDelete" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title d-inline">Delete Product</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="message">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <form action="" id="url" method="POST" class="d-inline">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal Delete -->


@endsection

@section('script')
    <script>
        function deleteConfirm(id, name){
            var url = '{{ route("produk.destroy", ":id") }}';
            url = url.replace(':id', id);
            document.getElementById("url").setAttribute("action", url);
            document.getElementById('message').innerHTML ="Are you sure want to delete Product<b>"+name+"</b> ?"
            $('#modalDelete').modal();
        }

    </script>
@endsection

