@extends('layouts.admin')

@section('title', 'Dashboard')

@section('breadcrumbs', 'Dashboard')

@section('css')
    <link href="https://cdn.jsdelivr.net/npm/chartist@0.11.0/dist/chartist.min.css" rel="stylesheet">

    <style>
        .traffic-chart {
            min-height: 335px;
        }
    </style>
@endsection

@section('content')
@endsection

@section('script')
@endsection
