@extends('layouts.admin')

@section('title', 'Edit About')

@section('breadcrumbs', 'About')

@section('second-breadcrumb')
    <li> Edit About</li>
@endsection

@section('content')
    <!-- table  -->
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">

                    @if (session('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{session('success')}}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                    @endif

                    <h3 class="text-center mt-3 mb-5">Edit</h3>

                    <div class="row">
                        <div class="col-3 mt-4">
                            <div class="card shadow">
                                @if($about->title == 'MISI')
                                    <img hidden src="{{asset('about_image/'.$about->image)}}" class="card-img-top"
                                         alt="image">
                                @else
                                    <img src="{{asset('about_image/'.$about->image)}}" class="card-img-top" alt="image">
                                @endif

                            </div>
                        </div>
                        <div class="col-9">
                            <form action="{{route('abouts.update', [$about->id])}}" method="POST" class="d-inline"
                                  enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                <p>{{$about->title}}</p>
                                <p>
                                    <textarea name="caption" id="content" rows="10"
                                              class="form-control">{{$about->caption}}</textarea>
                                </p>
                                <div class="row">
                                    <div class="d-inline col-6">
                                        @if($about->title == 'MISI')
                                            <p hidden>Image : 450 x 300px</p>
                                            <input type="file" name="image" hidden>
                                        @else
                                            <p>Image : 450 x 300px</p>
                                            <input type="file" name="image">
                                        @endif
                                    </div>
                                </div>
                                <div class="col-5 d-inline">
                                    <button type="submit" class="btn btn-success pull-right"> Update</button>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- /table -->
@endsection

@section('script')
@endsection

